package controllers;

import is.rufan.user.domain.User;
import play.*;
import play.mvc.*;

import views.html.*;

/**
 * Loads the front page
 */
public class Application extends Controller {

    /**
     * Loads the front page
     * @return
     */
    public Result index()
    {
        return ok(index.render("RuFan"));
    }

}
