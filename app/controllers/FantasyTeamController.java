package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.player.service.PlayerService;
import is.rufan.tournament.domain.FantasyTeam;
import is.rufan.tournament.domain.FantasyTeamListItem;
import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.service.FantasyTeamService;
import is.rufan.tournament.service.NonexistingPlayerException;
import is.rufan.tournament.service.TournamentService;
import is.rufan.user.domain.User;
import is.rufan.user.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.fantasyteam;
import views.html.fantasyteamlist;

import java.util.ArrayList;
import java.util.List;

import static play.libs.Json.toJson;

/**
 * Handles request to fantasy teams
 * Created by Omar on 23.10.2015.
 */
public class FantasyTeamController extends Controller {
    protected ApplicationContext ctx = new FileSystemXmlApplicationContext("/conf/tournamentapp.xml");
    protected ApplicationContext playerCtx = new FileSystemXmlApplicationContext("/conf/playerapp.xml");

    /**
     * Renders the fantasy team main page
     * @return
     */
    public Result fantasyTeamOverview () { return ok(fantasyteamlist.render()); }


    public Result fantasyTeam (int tournamentId) {
        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");
        Tournament tournamentObj = tournamentService.getTournament(tournamentId);
        if (tournamentObj == null) {
            return notFound();
        }

        int userId = Integer.parseInt(session().get("userId"));
        List<User> usersInTournament = tournamentService.getUsersInTournament(tournamentId);
        boolean userInTournament = false;
        for (User u : usersInTournament) {
            if (u.getId() == userId)
                userInTournament = true;
        }

        return ok(fantasyteam.render(tournamentObj, userInTournament));
    }

    public Result getFantasyTeam (int tournamentId) {
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");

        // Get the current userId
        String userId = session().get("userId");
        // get the detailed fantasy team
        FantasyTeam fantasyTeam = null;
        try {
            fantasyTeam = fantasyTeamService.getFantasyTeam(Integer.parseInt(userId), tournamentId);
        } catch (Exception ex) {
            // return an empty list
            return ok(toJson(new Object[]{}));
        }

        return ok(toJson(fantasyTeam));
    }

    public Result getFantasyTeamList() {
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");

        // get the current userId
        String userId = session().get("userId");
        List<FantasyTeamListItem> fantasyTeams = fantasyTeamService.getActiveFantasyTeamList(Integer.parseInt(userId));

        return ok(toJson(fantasyTeams));
    }

    public Result getPlayersByPosition(int positionId) {
        PlayerService playerService = (PlayerService) playerCtx.getBean("playerService");
        List<PlayerWithTeamName> playersInPosition = playerService.getPlayersByPosition(positionId);

        return ok(toJson(playersInPosition));
    }

    public Result addPlayer(int tournamentId) {
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");
        int userId = Integer.parseInt(session().get("userId"));

        JsonNode json = request().body().asJson();
        if (json == null) {
            return badRequest("Expecting json data");
        }

        int playerId = json.findPath("playerId").intValue();
        int positionId = json.findPath("positionId").intValue();
        /* System.out.println("tournamentId " + tournamentId);
        System.out.println("playerId: " + playerId);
        System.out.println("positionId: " + positionId); */

        try {
            fantasyTeamService.addPlayerToFantasyTeam(userId, tournamentId, playerId, positionId);
        } catch (Exception ex) {
            return badRequest(ex.getMessage());
        }

        /* PlayerService playerService = (PlayerService) playerCtx.getBean("playerService");
        Player player = playerService.getPlayer(playerId); */

        return ok();
    }

    public Result removePlayer(int tournamentId) {
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");
        int userId = Integer.parseInt(session().get("userId"));

        JsonNode json = request().body().asJson();
        if (json == null) {
            return badRequest("Expecting json data");
        }

        int playerId = json.findPath("playerId").intValue();

        try {
            fantasyTeamService.removePlayerFromFantasyTeam(userId, tournamentId, playerId);
        } catch (Exception ex) {
            return badRequest(ex.getMessage());
        }

        return ok();
    }

    public Result updateTeamName(int tournamentId) {
        FantasyTeamService fantasyTeamService = (FantasyTeamService) ctx.getBean("fantasyTeamService");
        int userId = Integer.parseInt(session().get("userId"));

        JsonNode json = request().body().asJson();
        if (json == null) {
            return badRequest("Expecting json data");
        }

        String name = json.findPath("teamName").textValue();

        try {
            fantasyTeamService.updateFantasyTeamName(userId, tournamentId, name);
        } catch (Exception ex) {
            return badRequest(ex.getMessage());
        }

        return ok();
    }
}
