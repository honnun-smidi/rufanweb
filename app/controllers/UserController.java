package controllers;


import is.rufan.team.domain.Team;
import is.rufan.team.service.TeamService;
import is.rufan.user.domain.CreditCardType;
import is.rufan.user.domain.User;
import is.rufan.user.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.profile;

import java.util.HashMap;
import java.util.Map;

import static play.data.Form.form;

public class UserController extends Controller
{
    protected ApplicationContext userCtx = new FileSystemXmlApplicationContext("/conf/userapp.xml");
    protected ApplicationContext teamCtx = new FileSystemXmlApplicationContext("/conf/teamapp.xml");

    public Result profile() {
        String username = session().get("username");
        UserService userService = (UserService) userCtx.getBean("userService");

        User user = userService.getUserByUsername(username);
        String favTeamId = "none";
        if (user.getFavoriteTeam() != null)
            favTeamId = Integer.toString(user.getFavoriteTeam().getTeamId());

        Form<User> profileForm = form(User.class);
        profileForm = profileForm.fill(user);

        return ok(profile.render(profileForm, favTeamId));
    }

    public Result submit() {
        Form<User> profileForm = form(User.class);
        Form<User> updatedForm = profileForm.bindFromRequest();

        String favTeamId = updatedForm.field("favoriteTeamId").value();
        boolean creditCardInformation = false;

        // Validate updated form data
        if (updatedForm.field("name").value().length() < 1)
            updatedForm.reject("name", "Name cannot be empty.");
        if (updatedForm.field("email").value().length() < 1)
            updatedForm.reject("email", "Email cannot be empty.");

        // If the credit card number field is not empty
        if (updatedForm.field("creditCardNumber").value().length() > 0 ) {
            if (updatedForm.field("creditCardNumber").value().length() != 16 ||
                    !isNumeric(updatedForm.field("creditCardNumber").value())) {
                updatedForm.reject("creditCardNumber", "Not a valid credit card number");
            }
            if (updatedForm.field("creditCardType").value().equals("none")) {
                updatedForm.reject("creditCardType", "Please choose a credit card type");
            }
            if (updatedForm.field("creditCardExpMonth").value().equals("none")) {
                updatedForm.reject("creditCardExpMonth", "You must choose an expiration month ");
            }
            if (updatedForm.field("creditCardExpYear").value().equals("none")) {
                updatedForm.reject("creditCardExpYear", "You must choose an expiration year");
            }

            creditCardInformation = true;
        }

        if (updatedForm.hasErrors()) {
            return badRequest(profile.render(updatedForm, favTeamId));
        }

        UserService userService = (UserService) userCtx.getBean("userService");
        TeamService teamService = (TeamService) teamCtx.getBean("teamService");
        String username = session().get("username");
        User currentUser = userService.getUserByUsername(username);
        User updatedUser = updatedForm.get();

        currentUser.setName(updatedUser.getName());
        currentUser.setEmail(updatedUser.getEmail());
        currentUser.setCreditCardNumber(updatedUser.getCreditCardNumber());
        if (creditCardInformation) {
            String creditCardType = updatedForm.field("creditCardType").value();
            String creditCardExpMonth = updatedForm.field("creditCardExpMonth").value();
            String creditCardExpYear = updatedForm.field("creditCardExpYear").value();
            currentUser.setCreditCardType(CreditCardType.valueOf(creditCardType));
            currentUser.setCreditCardExpMonth(Integer.parseInt(creditCardExpMonth));
            currentUser.setCreditCardExpYear(Integer.parseInt(creditCardExpYear));
        }

        // set favorite team if one is selected
        if (!favTeamId.equals("none")) {
            Team team = teamService.getTeam(Integer.valueOf(favTeamId));
            currentUser.setFavoriteTeam(team);
        }

        userService.updateUser(currentUser);
        User fullyUpdatedUser = userService.getUser(currentUser.getId());
        updatedForm = updatedForm.fill(fullyUpdatedUser);

        return ok(profile.render(updatedForm, favTeamId));
    }

    private boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
