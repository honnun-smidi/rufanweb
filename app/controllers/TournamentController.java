package controllers;

import is.rufan.tournament.domain.Tournament;
import is.rufan.tournament.domain.TournamentStatus;
import is.rufan.tournament.service.TournamentService;
import is.rufan.user.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.tournament;
import views.html.tournaments;

import java.util.List;

import static play.libs.Json.toJson;

/**
 * Created by Omar on 23.10.2015.
 */
public class TournamentController extends Controller {
    protected ApplicationContext ctx = new FileSystemXmlApplicationContext("/conf/tournamentapp.xml");

    public Result tournamentOverview() {
        return ok(tournaments.render());
    }

    public Result allTournaments() {
        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");
        List<Tournament> tournaments = tournamentService.getTournaments();

        return ok(toJson(tournaments));
    }

    public Result tournamentsByStatus(int status) {
        TournamentStatus tournamentStatus = TournamentStatus.fromInt(status);

        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");
        List<Tournament> tournaments = tournamentService.getTournamentsByStatus(tournamentStatus);

        return ok(toJson(tournaments));
    }

    public Result registerToTournament(int tournamentId) {
        TournamentService tournamentService = (TournamentService) ctx.getBean("tournamentService");
        int userId = Integer.parseInt(session().get("userId"));

        try {
            tournamentService.addUserToTournament(tournamentId, userId);
        } catch (Exception ex) {
            return badRequest(ex.getMessage());
        }

        return ok();
    }
}