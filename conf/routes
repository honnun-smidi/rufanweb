# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# Home page
GET         /                                                    controllers.Application.index()
GET         /signup                                              controllers.SignupController.blank()
POST        /signup                                              controllers.SignupController.signup()
GET         /login                                               controllers.LoginController.blank()
POST        /login                                               controllers.LoginController.login()
GET         /logout                                              controllers.LoginController.logout()

# User
GET         /profile                                             controllers.UserController.profile()
POST        /profile                                             controllers.UserController.submit()

# Team
GET         /api/teams                                           controllers.TeamController.list()

# Tournaments
GET         /tournaments                                         controllers.TournamentController.tournamentOverview()
GET         /api/tournaments                                     controllers.TournamentController.allTournaments()
GET         /api/tournaments/:status                             controllers.TournamentController.tournamentsByStatus(status: Integer)
POST        /api/tournaments/:tournamentId                       controllers.TournamentController.registerToTournament(tournamentId: Integer)

# FantasyTeams
GET         /fantasyteams                                        controllers.FantasyTeamController.fantasyTeamOverview()
GET         /fantasyteam/:tournamentid                           controllers.FantasyTeamController.fantasyTeam(tournamentid: Integer)
GET         /api/fantasyteamlist                                 controllers.FantasyTeamController.getFantasyTeamList()
GET         /api/fantasyteam/:tournamentid                       controllers.FantasyTeamController.getFantasyTeam(tournamentid: Integer)
GET         /api/fantasyteam/playersposition/:posid              controllers.FantasyTeamController.getPlayersByPosition(posid: Integer)
POST        /api/fantasyteam/:tournamentid/addplayer             controllers.FantasyTeamController.addPlayer(tournamentid: Integer)
POST        /api/fantasyteam/:tournamentid/removeplayer          controllers.FantasyTeamController.removePlayer(tournamentid: Integer)
POST        /api/fantasyteam/:tournamentid/updateteamname        controllers.FantasyTeamController.updateTeamName(tournamentid: Integer)

# Map static resources from the /public folder to the /assets URL path
GET         /assets/*file                                        controllers.Assets.versioned(path="/public", file: Asset)

