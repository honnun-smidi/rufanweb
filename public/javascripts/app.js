/**
 * Created by Snaebjorn on 10/22/2015.
 */
var app = {
    init: function () {
        this.profile();
        this.tournament();
        this.fantasyTeams();
        this.fantasyTeam();
    },

    profile: function () {
        var $favTeamDropdown = $('#favoriteTeamId');
        if ($favTeamDropdown.length > 0) {
            // get team list and populate dropdown
            $.ajax({
                type: 'GET',
                url: 'api/teams',
                success: function (data) {
                    for (var i = 0; i < data.length; i++) {
                        $favTeamDropdown
                            .append($('<option />')
                            .val(data[i].teamId)
                            .text(data[i].displayName))
                    }
                    // set user's favorite team as selected option
                    $favTeamDropdown.val($favTeamDropdown.attr('data-team-id'));
                }
            });
            var $creditCardNumber = $('#creditCardNumber');
            var $creditCardInfo = $('#creditCardInfo');
            var $creditCardExpYear = $('#creditCardExpYear');
            var $creditCardExpMonth = $('#creditCardExpMonth');
            var $creditCardType = $('#creditCardType');
            // If the credit card number is empty, hide 
            // other credtit card info
            if ($creditCardNumber.val().length <= 0) {
                $creditCardInfo.hide();
            } else {
                $creditCardInfo.show();
            }
            // Listens to credit card number. If it is a valid
            // credit card number (16 char length) then show
            // more credit card information
            $creditCardNumber.on('keyup', function(e) {
                if ($creditCardNumber.val().length > 0) {
                    console.log("16");
                    $creditCardInfo.show();
                } else {
                    $creditCardInfo.hide();
                }
            });
        }

    },

    tournament: function () {
        var $tournamentList = $('#tournamentList');
        // Get active tournaments
        if ($tournamentList.length > 0) {
            $.ajax({
                type: 'GET',
                url: 'api/tournaments/0',
                success: function (data) {
                    // Append active tournaments to table
                    for (var i = 0; i < data.length; i++) {
                        $tournamentList
                            .append($('<tr>'
                                    + '<td><a href="fantasyteam/' + data[i].id + '">' + data[i].name + '</a></td>'
                                    + '<td>' + data[i].leagueName + '</td>'
                                    + '<td>' + data[i].startTime + '</td>'
                                    + '<td>' + data[i].endTime +'</td>'
                                    + '</tr>'));
                    }
                }
            });
        }
    },

    fantasyTeams: function () {
        var $fantasyTeamsList = $('#fantasyTeams');
        // Get fantasy teams
        if ($fantasyTeamsList.length > 0) {
            $.ajax({
                type: 'GET',
                url: 'api/fantasyteamlist',
                success: function (data) {
                    // Append fantasy teams to list
                    for (var i = 0; i < data.length; i++) {
                        var fantasyTeamLink = '/fantasyteam/' + data[i].tournament.id;
                        $fantasyTeamsList
                            .append($('<tr>'
                                    + '<td><a href="' + fantasyTeamLink + '">' 
                                    + data[i].teamName + '</a></td>'
                                    + '<td><a href="#">' + data[i].tournament.name + '</a></td>' ));
                    }
                }
            });
        }
    },

    fantasyTeam: function () {
        var $fantasyTeamPlayers = $('#fantasyTeamPlayers');

        if ($fantasyTeamPlayers.length > 0) {
            var $goalkeeperTable = $fantasyTeamPlayers.find('#goalkeeper');
            var $defendersTable = $fantasyTeamPlayers.find('#defenders');
            var $midfieldersTable = $fantasyTeamPlayers.find('#midfielders');
            var $forwardsTable = $fantasyTeamPlayers.find('#forwards');
            var tournamentId = $fantasyTeamPlayers.data('tournament-id');
            var $teamNameInput = $('#teamName');

            // Append selected player to correct position table
            var appendPlayer = function (player) {
                var playerName = (player.firstName) ? player.firstName + ' ' : '';
                playerName += player.lastName;
                var $tableRow = $('<tr>' +
                                '<td>' + playerName + '</td>' +
                                '<td>' + player.teamName + '</td>' +
                                '<td>' + player.nationality.name + '</td>' +
                                '<td><button data-player-id="' + player.playerId + '" class="btn btn-danger btn-xs">X</button>' +
                                '</tr>');
                if (player.positions[0].positionId === 1)
                    $goalkeeperTable.append($tableRow);
                else if (player.positions[0].positionId === 2)
                    $forwardsTable.append($tableRow);
                else if (player.positions[0].positionId === 3)
                    $midfieldersTable.append($tableRow);
                else
                    $defendersTable.append($tableRow);
            };

            // Get players in this fantasy team and append it
            // to appropriate position table
            $.ajax({
                type: 'GET',
                url: '/api/fantasyteam/' + tournamentId,
                success: function (data) {
                    console.dir(data);
                    var teamName = (data.teamName) ? data.teamName : "No name";
                    $('#fantasyTeamName').html(teamName);
                    for (var i = 0; i < data.players.length; i++) {
                        appendPlayer(data.players[i]);
                    }
                }
            });
            
            var $selectPlayerPosition = $('#selectPlayerPosition');

            // Append players to available players table
            var loadPlayers = function($container, data) {
                // Clear the available players table
                $container.find('tbody > tr').remove();
                for (var i = 0; i < data.length; i++) {
                    var playerName = (data[i].firstName) ? data[i].firstName + ' ' : '';
                    playerName += data[i].lastName;
                    var teamName = data[i].teamName;
                    var playerId = data[i].playerId;
                    var positionId = data[i].positions[0].positionId;
                    var nationality = data[i].nationality.name;

                    $container.append('<tr data-player-id="' + playerId +
                                                '" data-position-id="' + positionId + '">' +
                                                '<td>' + playerName + '</td>' +
                                                '<td>' + teamName + '</td>' + 
                                                '<td>' + nationality + '</td>' +
                                            '</tr>');
                }
            };

            // Load the players that play in the selected position
            var $playersInPosition = $('#playersInPosition');
            $.ajax({
                type: 'GET',
                url: '/api/fantasyteam/playersposition/' + $selectPlayerPosition.val(),
                success: function (data) {
                    loadPlayers($playersInPosition, data);
                },
                error: function() {
                    toastr.error("Could not load players");
                }
            });

            // Listen to select player position changes and load
            // players accordingly
            $selectPlayerPosition.change(function () {
                $.ajax({
                    type: 'GET',
                    url: '/api/fantasyteam/playersposition/' + $selectPlayerPosition.val(),
                    success: function (data) {
                        loadPlayers($playersInPosition, data);
                    }
                });
            });

            // Listen to players postion table on click and append the player 
            // to selected players table 
            $('body').on('click', '#playersInPosition > tbody > tr', function (e) {
                console.log($(this).data('player-id'));
                $(this).attr('data-playerId');
                var positionId = $(this).data('position-id');
                var $tableRow = $('<tr>' + $(this).html() + '<td><button class="btn btn-danger btn-xs">X</button>' +  '</tr>');

                $.ajax({
                    type: 'POST',
                    url: '/api/fantasyteam/' + tournamentId + '/addplayer',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        playerId:   $(this).data('player-id'),
                        positionId: positionId
                    }),
                    success: function () {
                        // Add to correct table
                        if (positionId === 1)
                            $goalkeeperTable.append($tableRow);
                        else if (positionId === 2)
                            $forwardsTable.append($tableRow);
                        else if (positionId === 3)
                            $midfieldersTable.append($tableRow);
                        else
                            $defendersTable.append($tableRow);
                    },
                    error: function (err) {
                        toastr.error(err.responseText);
                    }
                });
            });
            
            // Remove player from fantasy team
            $('body').on('click', 'table > tbody > tr > td > button', function (e) {
                var $tableRowToRemove = $(this).parent().parent();

                $.ajax({
                    type: 'POST',
                    url: '/api/fantasyteam/' + tournamentId + '/removeplayer',
                    contentType: 'application/json',
                    data: JSON.stringify({
                        playerId: $(this).data('player-id')
                    }),
                    success: function () {
                        toastr.success("Removed player");
                        $tableRowToRemove.remove();
                    },
                    error: function () {
                        toastr.error("Could not remove player");
                    }
                });
            });

            // Change team name
            $('#changeTeamName').click (function () {
                var teamName = $teamNameInput.val();
                console.log('team name change - ' + teamName);
                if (teamName != "") {
                    $.ajax({
                        type: 'POST',
                        url: '/api/fantasyteam/' + tournamentId + '/updateteamname',
                        contentType: 'application/json',
                        data: JSON.stringify({
                            teamName: teamName
                        }),
                        success: function () {
                            toastr.success("Team name changed to: " + teamName);
                            $('#fantasyTeamName').html(teamName);
                        },
                        error: function (err) {
                            toastr.error(err.responseText);
                        }
                    });
                } else {
                    toastr.error("Fantasy team name cannot be empty")
                }
            });
        }
        // Register the user to a given tournament
        $('#registerToTournament').click(function (e) {
            var tournamentId = $(this).data('tournament-id');
                $.ajax({
                    type: 'POST',
                    url: '/api/tournaments/' + tournamentId,
                    success: function () {
                        toastr.success("You are registered to tournament");
                        // Reload the page to get fantasy team view
                        location.reload();
                    },
                    error: function (err) {
                        toastr.error(err.responseText);
                    }
                });
        });
    }
};

$(document).ready(function () {
    app.init();
});
